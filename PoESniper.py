#!/usr/bin/env python
# pylint: disable=C
# pylint: disable=W0703

from Tkinter import *
import Queue
import ItemParser
import winsound
import json
import ast
import time


class InputDialog(object):
    def __init__(self, master, prompt):
        top = self.top = Toplevel(master)
        self.value = ''
        self.label = Label(top, text=prompt)
        self.label.pack(side=LEFT)
        self.entry = Entry(top, exportselection=False)
        self.entry.pack(fill=Y, side=LEFT, pady=5)
        self.entry.bind('<Return>', self.returnInput)
        self.entry.focus()
        self.button = Button(top, text='Ok')
        self.button.bind('<Button-1>', self.returnInput)
        self.button.pack(fill=X, side=LEFT, padx=5)

    def returnInput(self, event):
        self.value = self.entry.get()
        self.top.destroy()

class InfoDialog(object):
    def __init__(self, master, info):
        top = self.top = Toplevel(master)
        self.label = Label(top, text=info, justify=LEFT)
        top.minsize(width=256, height=128)
        self.label.pack(padx=20, pady=10)

class GUI(object):
    def __init__(self, master):
        self.master = master
        self.master.lift()
        self.master.geometry('512x384')
        self.master.protocol('WM_DELETE_WINDOW', self.onClose)

        self.queue = Queue.Queue()
        self.sniper = ItemParser.ItemParser(self.queue)
        self.lastBeep = time.time() + 1
        self.whisperList = []
        self.unpricedFlag = IntVar()
        self.unpricedFlag.set(1)
        self.soundFlag = IntVar()
        self.soundFlag.set(1)

        menu = Menu(self.master)
        self.master.config(menu=menu)

        subMenu = Menu(menu, tearoff=False)
        menu.add_cascade(label="Options", menu=subMenu)
        menu.add_command(label='About', command=self.showAbout)
        menu.add_separator()
        menu.add_command(label="Clear", command=self.clearItemList)
        subMenu.add_command(label='League...', command=self.updateLeague)
        subMenu.add_command(label='Search...', command=self.updateItemSearch)
        subMenu.add_command(label='Max Price...',
                            command=self.updateMaxPrice)
        subMenu.add_separator()
        subMenu.add_checkbutton(label='Unpriced Items', command=self.updateUnpriced, variable=self.unpricedFlag)
        subMenu.add_checkbutton(label='Sound Alert', command=self.updateSound, variable=self.soundFlag)

        self.statusBar = Label(self.master, text='Status bar...',
                          bd=1, relief=SUNKEN, anchor=W)
        self.statusBar.pack(side=BOTTOM, fill=X)

        leftFrame = Frame(self.master)
        leftFrame.pack(side=LEFT, fill=Y)

        rightFrame = Frame(self.master)
        rightFrame.pack(side=RIGHT, fill=BOTH, expand=1)

        scrollbarY = Scrollbar(rightFrame)
        scrollbarY.pack(side=RIGHT, fill=Y)

        self.listbox = Listbox(rightFrame)
        self.listbox.config(font=('Courier', 10))
        self.listbox.config(activestyle='none')
        self.listbox.pack(fill=BOTH, expand=1)

        self.listbox.config(yscrollcommand=scrollbarY.set)
        scrollbarY.config(command=self.listbox.yview)

        self.listbox.bind('<<ListboxSelect>>', self.listboxChanged)

        self.updateStatusBar()
        self.sniper.start()
        self.processQueue()

    def showAbout(self):
        self.showInfoDialog('PoE Sniper\n\n' +
                            'Syntax for item search:\n' +
                            '   - Search for all item names + bases containing jewel: "jewel"\n' +
                            '   - Search for all item names + bases containing jewel and NOT cobalt: "jewel!cobalt"\n'
                            '   - Note: spaces are significant, while case is not.\n' +
                            '   - For instance, "ring" and " ring" are different; "ring" matches "offering" while " ring" does not.')

    def clearItemList(self):
        self.whisperList = []
        self.listbox.delete(0, END)

    def updateStatusBar(self):
        self.statusBar.config(text='%s | %s | %s | %d chaos | %s | %s' % (self.sniper.targetLeague, self.sniper.targetItem, '!' + self.sniper.notTargetItem, self.sniper.maxPrice, self.unpricedFlag.get(), self.soundFlag.get()))

    def updateLeague(self):
        s = self.showInputDialog('Enter league: ')
        if s != '':
            self.sniper.targetLeague = s.lower()
        self.updateStatusBar()

    def updateItemSearch(self):
        s = self.showInputDialog('Enter item search: ').split('!')
        if s[0] != '':
            self.sniper.targetItem = s[0]
        if(len(s) > 1):
            self.sniper.notTargetItem = s[1]
        self.updateStatusBar()

    def updateMaxPrice(self):
        try:
            s = self.showInputDialog('Enter max price (or -1):')
            if s == '':
                s = '-1'
            self.sniper.maxPrice = float(s)
        except Exception:
            print 'Invalid value entered.'
            return
        self.updateStatusBar()

    def updateUnpriced(self):
        self.sniper.unpricedFlag = self.unpricedFlag.get()
        self.updateStatusBar()

    def updateSound(self):
        self.updateStatusBar()

    def showInputDialog(self, prompt):
        d = InputDialog(self.master, prompt)
        self.master.wait_window(d.top)
        return d.value

    def showInfoDialog(self, info):
        d = InfoDialog(self.master, info)
        self.master.wait_window(d.top)

    def listboxChanged(self, e):
        wid = e.widget
        if(wid.size() == 0):
            return
        index = int(wid.curselection()[0])
        # print self.whisperList[index]
        self.master.clipboard_clear()
        self.master.clipboard_append(self.whisperList[index])

    def onClose(self):
        self.master.destroy()
        self.sniper.stop()

    def processQueue(self):
        try:
            item = self.queue.get(0)
            s = '%s | @%s | %s' % (item['price'] if item['price'] !=
                                   '' else 'unpriced', item['characterName'], item['itemName'])
            self.listbox.insert(0, s)
            self.whisperList.insert(0, '@%s Hi, I would like to buy your %s %sin %s (stash tab "%s"; position: left %d, top %d\n)'
                                    % (item['characterName'], item['itemName'], 'listed for ' + item['price'] + ' ' if item['price'] != '' else '', item['league'], item['tabName'], item['left'], item['top']))
            if(self.soundFlag.get() and self.lastBeep + 1 < time.time()):
                winsound.Beep(700, 100)
                self.lastBeep = time.time()
            # self.listbox.yview_moveto(1)
            if(len(self.whisperList) > 100):
                del self.whisperList[100:]
                self.listbox.delete(100, END)
            self.master.after(0, self.processQueue)
        except Queue.Empty:
            self.master.after(100, self.processQueue)


if __name__ == '__main__':
    root = Tk()
    root.title('PoE Sniper')
    gui = GUI(root)
    root.mainloop()
