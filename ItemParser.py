#!/usr/bin/env python
# pylint: disable=C
# pylint: disable=W0703

import urllib2
import json
import re
import time
import threading

class ItemParser(threading.Thread):
    POE_NINJA_URL = 'http://api.poe.ninja/api/Data/GetStats'
    POE_STASH_URL = 'http://api.pathofexile.com/public-stash-tabs?id='

    targetLeague = 'legacy'
    targetItem = None
    notTargetItem = ''
    maxPrice = -1
    unpricedFlag = 1

    changeId = ''
    data = None

    def getReq(self, url):
        return json.loads(urllib2.urlopen(url).read())

    def processData(self):
        stashes = self.data.get('stashes')
        if stashes == None:
            return

        for stash in stashes:
            for item in stash['items']:
                if self.targetItem != None and self.targetLeague == item['league'].lower() and (self.targetItem in item['name'].lower() or self.targetItem in item['typeLine'].lower()) and (not (self.notTargetItem in item['name'].lower() or self.notTargetItem in item['typeLine'].lower()) or self.notTargetItem == ''):
                    itemMatch = {}
                    itemMatch['characterName'] = stash['lastCharacterName']
                    itemMatch['tabName'] = stash['stash'].strip()
                    itemMatch['itemName'] = re.sub(r'<.*>', '', item['name']).strip()
                    itemMatch['baseType'] = re.sub(r'<.*>', '', item['typeLine']).strip()
                    itemMatch['itemName'] = itemMatch['itemName'] + (' ' if itemMatch['baseType'] != None else '') + itemMatch['baseType']
                    itemMatch['note'] = re.sub(r'~\S*\s', '', item.get('note', '')).strip()
                    itemMatch['price'] = itemMatch['note']
                    itemMatch['left'] = item['x']
                    itemMatch['top'] = item['y']
                    itemMatch['league'] = item['league']
                    if itemMatch['note'] == '' and ('~b/o' in itemMatch['tabName'] or '~price' in itemMatch['tabName']):
                        itemMatch['price'] = re.sub(r'~\S*\s', '', itemMatch['tabName'])
                    if itemMatch['price'] != '':
                        num = [float(s) for s in re.findall(r'\d+', itemMatch['price'])]
                        if len(num) != 0 and 'chaos' in itemMatch['price']:
                            if len(num) > 1 and (num[0] / num[1]) > self.maxPrice and self.maxPrice != -1:
                                continue
                            elif len(num) == 1 and self.maxPrice != -1 and num[0] > self.maxPrice:
                                continue
                    elif not self.unpricedFlag:
                        continue
                    self.queue.put(itemMatch)

    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue
        self.running = True
        while self.changeId == '':
            self.poeNinjaData = self.getReq(self.POE_NINJA_URL)
            self.changeId = self.poeNinjaData['nextChangeId']

        while self.data == None:
            self.data = self.getReq(self.POE_STASH_URL + self.changeId)
            self.changeId = self.data['next_change_id']

    def run(self):
        while self.running:
            # try:
            startTime = time.time()
            if self.data != None:
                self.processData()
            try:
                self.data = self.getReq(self.POE_STASH_URL + self.changeId)
                self.changeId = self.data['next_change_id']
            except Exception:
                self.data = None
                self.changeId = ''
                while self.changeId == '':
                    self.poeNinjaData = self.getReq(self.POE_NINJA_URL)
                    self.changeId = self.poeNinjaData['nextChangeId']
            elapsedTime = time.time() - startTime
            if elapsedTime < 1:
                time.sleep(1 - elapsedTime)
                # print '%f\n' % (time.time() - startTime)
            # except Exception, e:
            #     print 'Error in main loop: ' + str(e)
            #     break

    def stop(self):
        self.running = False
